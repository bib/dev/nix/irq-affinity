{ pkgs, lib, config, options, ... }: with lib; with lib.strings; let
  cfg = config.boot.irqAffinity;
in {
  options.boot.irqAffinity = with types; {
    enable = mkEnableOption "IRQ affinity override";
    default = mkOption { type = str; default = ""; };
    override = mkOption { type = str; default = ""; };
    overrides = mkOption { default = {}; };
  };

  config = mkIf cfg.enable {
    systemd.services."set-irq-affinity" = {
      description = "IRQ affinity configuration";
      serviceConfig = {
        Type = "oneshot"; RemainAfterExit = true;
      };
      wantedBy = [ "sysinit.target" ];
      script = let
        setAffinity = (irq: aff: "echo ${aff} > /proc/irq/${irq}/smp_affinity");
        in (concatStringsSep "\n" ([ ''
          set +e
        '' (optionalString (cfg.default != "") ''
          echo ${cfg.default} > /proc/irq/default_smp_affinity
        '') (optionalString (cfg.override != "") ''
          ${pkgs.findutils}/bin/find /proc/irq -name smp_affinity | while read path; do
            echo ${cfg.override} > $path
          done
        '') ] ++ (
          attrsets.mapAttrsToList setAffinity cfg.overrides
        ) ++ [ "exit 0"]));
    };
  };
}
